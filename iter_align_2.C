//double stability=0.1;
TMatrixD getMatrix1(double tanphi, double tantheta,double x, double y, double xe, double ye){
  TMatrixD m1(2,2);
  m1[0][0]=-1./xe;
  m1[0][1]=0.;
  m1[1][0]=0.;
  m1[1][1]=-1./ye;
  TMatrixD m2(2,2);
  TMatrixD m3(2,2);
  m2.Transpose(m1);
  m3=m1*m2;
  return m3;
}

TMatrixD getMatrix2(double resx,double resy, double tanphi, double tantheta,double x, double y, double xe, double ye){
  TMatrixD m1(2,2);
  m1[0][0]=-1./xe;
  m1[0][1]=0.;
  m1[1][0]=0.;
  m1[1][1]=-1./ye;
  TMatrixD m2(2,1);
  m2[0][0]=resx/xe;
  m2[1][0]=resy/ye;
  TMatrixD m3(2,1);
  m3=m1*m2;
  return m3;
}

void iter_align_2(){ 
  TFile* f1=new TFile("../../iter0/trackerspfitreader_iter_0.root"); 
  TTree* t1=(TTree*)f1->Get("spfit");
  ofstream output;
  output.open("iteralign_2.txt");
  int nsp=0;
  std::vector<double>* m_sp_track_x_residual=0;
  std::vector<double>* m_sp_track_y_residual=0;
  std::vector<double>* m_sp_track_z_residual=0;
  std::vector<double>* m_sp_track_x_expected=0;
  std::vector<double>* m_sp_track_y_expected=0;
  std::vector<double>* m_sp_track_z_expected=0;
  std::vector<double>* m_sp_track_x_measured=0;
  std::vector<double>* m_sp_track_y_measured=0;
  std::vector<double>* m_sp_track_z_measured=0;
  std::vector<double>* m_sp_track_x_err=0;
  std::vector<double>* m_sp_track_y_err=0;
  std::vector<int>* m_sp_track_station=0;
  std::vector<int>* m_sp_track_module=0;
  std::vector<int>* m_sp_track_layer=0;
  std::vector<double>* m_track_p0=0;
  std::vector<double>* m_track_p1=0;
  std::vector<double>* m_track_p2=0;
  std::vector<double>* m_track_p3=0;
  t1->SetBranchAddress("sp_track_x_err",&m_sp_track_x_err);
  t1->SetBranchAddress("sp_track_y_err",&m_sp_track_y_err);
  t1->SetBranchAddress("sp_track_x",&m_sp_track_x_measured);
  t1->SetBranchAddress("sp_track_y",&m_sp_track_y_measured);
  t1->SetBranchAddress("sp_track_z",&m_sp_track_z_measured);
  t1->SetBranchAddress("sp_track_x_residual",&m_sp_track_x_residual);
  t1->SetBranchAddress("sp_track_y_residual",&m_sp_track_y_residual);
  t1->SetBranchAddress("sp_track_z_residual",&m_sp_track_z_residual);
  t1->SetBranchAddress("sp_track_station",&m_sp_track_station);
  t1->SetBranchAddress("sp_track_module",&m_sp_track_module);
  t1->SetBranchAddress("sp_track_layer",&m_sp_track_layer);
  t1->SetBranchAddress("sp_track_layer",&m_sp_track_layer);

  double align_x[3]={0.};
  double align_y[3]={0.};
  double align_r[3]={0.};
  double align_mx[8]={0.};
  double align_my[8]={0.};
    TH1D* hx[3][8];//=new TH1D[8];
    TH1D* hy[3][8];//=new TH1D[8];
    TH1D* hz[3][8];//=new TH1D[8];
    TH1D* hn[3][8];//=new TH1D[8];
    TH1D* hx_layer[3];//=new TH1D[8];
    TH1D* hy_layer[3];//=new TH1D[8];
    TProfile* hx_x_layer[3];//=new TH1D[8];
    TProfile* hy_y_layer[3];//=new TH1D[8];
    TProfile* hx_y_layer[3];//=new TH1D[8];
    TProfile* hy_x_layer[3];//=new TH1D[8];
    TH1D* hn_aver=new TH1D(Form("numberOfSPs"),"number of SPs",20,0,20);
    TH1D* hx_track=new TH1D(Form("x_trackSPs"),"residual x of SPs",100,-10,10);
    TH1D* hy_track=new TH1D(Form("y_trackSPs"),"residual y of SPs",100,-0.1,0.1);
    TH1D* hz_track=new TH1D(Form("z_trackSPs"),"residual z of SPs",2500,-0,2500);
    TH2D* h_x_y_track=new TH2D(Form("x_y_trackSPs"),"residual x and y of SPs",100,-20,20,100,-20,20);
    TMatrixD denom_all_l0(2,2);
    TMatrixD num_all_l0(2,1);
    TMatrixD denom_all_l1(2,2);
    TMatrixD num_all_l1(2,1);
    TMatrixD denom_all_l2(2,2);
    TMatrixD num_all_l2(2,1);
    TMatrixD denom_all_l1m0(2,2);
    TMatrixD num_all_l1m0(2,1);
    TMatrixD denom_all_l1m1(2,2);
    TMatrixD num_all_l1m1(2,1);
    TMatrixD denom_all_l1m2(2,2);
    TMatrixD num_all_l1m2(2,1);
    TMatrixD denom_all_l1m3(2,2);
    TMatrixD num_all_l1m3(2,1);
    TMatrixD denom_all_l1m4(2,2);
    TMatrixD num_all_l1m4(2,1);
    TMatrixD denom_all_l1m5(2,2);
    TMatrixD num_all_l1m5(2,1);
    TMatrixD denom_all_l1m6(2,2);
    TMatrixD num_all_l1m6(2,1);
    TMatrixD denom_all_l1m7(2,2);
    TMatrixD num_all_l1m7(2,1);
    for(int i=0;i<2;i++){
      num_all_l0[i][0]=0.;
      num_all_l1[i][0]=0.;
      num_all_l1m0[i][0]=0.;
      num_all_l1m1[i][0]=0.;
      num_all_l1m2[i][0]=0.;
      num_all_l1m3[i][0]=0.;
      num_all_l1m4[i][0]=0.;
      num_all_l1m5[i][0]=0.;
      num_all_l1m6[i][0]=0.;
      num_all_l1m7[i][0]=0.;
      num_all_l2[i][0]=0.;
      for(int j=0;j<2;j++){
	denom_all_l0[i][j]=0.;
	denom_all_l1[i][j]=0.;
	denom_all_l1m0[i][j]=0.;
	denom_all_l1m1[i][j]=0.;
	denom_all_l1m2[i][j]=0.;
	denom_all_l1m3[i][j]=0.;
	denom_all_l1m4[i][j]=0.;
	denom_all_l1m5[i][j]=0.;
	denom_all_l1m6[i][j]=0.;
	denom_all_l1m7[i][j]=0.;
	denom_all_l2[i][j]=0.;
      }
    }
    for(int i=0;i<3;i++){
      hx_layer[i]=new TH1D(Form("x_station1_layer%d",i),"residual x",20,-10.0,10.0);
      hy_layer[i]=new TH1D(Form("y_station1_layer%d",i),"residual y",50,-0.50,0.50);
      for(int j=0;j<8;j++){
	std::cout<<"define hist"<<i<<" "<<j<<std::endl;
	hx[i][j]=new TH1D(Form("x_station1_layer%d_module%d",i,j),"residual x",50,-10.0,10.0);
	hy[i][j]=new TH1D(Form("y_station1_layer%d_module%d",i,j),"residual y",50,-0.50,0.50);
	hz[i][j]=new TH1D(Form("z_station1_layer%d_module%d",i,j),"residual z",2500,0,2500);
	hn[i][j]=new TH1D(Form("numberOfSPs_station1_layer%d_module%d",i,j),"number of SPs",20,0,20);
      }
    }
    std::cout<<"read events"<<std::endl;
    for(int ievt=0;ievt<t1->GetEntries();ievt++){
      t1->GetEntry(ievt);
      if(ievt%10000==1)std::cout<<"processing "<<ievt<<"  events"<<std::endl;

      int num[3][8]={0};
      for(int i=0;i<m_sp_track_x_measured->size();i+=3){
	int imodule=m_sp_track_module->at(i+1);
	double x0=m_sp_track_x_measured->at(i);
	double y0=m_sp_track_y_measured->at(i);
	double z0=m_sp_track_z_measured->at(i);
	double x1=m_sp_track_x_measured->at(i+1);
	double y1=m_sp_track_y_measured->at(i+1);
	//x1+=align_x[1];
	//y1+=align_y[1];
	//x1+=align_mx[imodule];
	//y1+=align_my[imodule];
	double z1=m_sp_track_z_measured->at(i+1);
	double x2=m_sp_track_x_measured->at(i+2);
	double y2=m_sp_track_y_measured->at(i+2);
	double z2=m_sp_track_z_measured->at(i+2);
	double xe0=m_sp_track_x_err->at(i);
	double ye0=m_sp_track_y_err->at(i);
	double xe1=m_sp_track_x_err->at(i+1);
	double ye1=m_sp_track_y_err->at(i+1);
	double xe2=m_sp_track_x_err->at(i+2);
	double ye2=m_sp_track_y_err->at(i+2);
	//for layer1
	double res_x_l1=x1-(x0+(z1-z0)/(z2-z0)*(x2-x0));
	double res_y_l1=y1-(y0+(z1-z0)/(z2-z0)*(y2-y0));
	double tanphi1=(x2-x0)/(z2-z0);
	double tantheta1=(y2-y0)/(z2-z0);
	TMatrixD m11=getMatrix1(tanphi1,tantheta1,x1,y1,xe1,ye1);
	TMatrixD m21=getMatrix2(res_x_l1,res_y_l1,tanphi1,tantheta1,x1,y1,xe1,ye1);
	denom_all_l1+=m11;
	num_all_l1+=m21;;
	  if(imodule==0){
	    denom_all_l1m0+=m11;
	    num_all_l1m0+=m21;;
	  }
	  if(imodule==1){
	    denom_all_l1m1+=m11;
	    num_all_l1m1+=m21;;
	  }
	  if(imodule==2){
	    denom_all_l1m2+=m11;
	    num_all_l1m2+=m21;;
	  }
	  if(imodule==3){
	    denom_all_l1m3+=m11;
	    num_all_l1m3+=m21;;
	  }
	  if(imodule==4){
	    denom_all_l1m4+=m11;
	    num_all_l1m4+=m21;;
	  }
	  if(imodule==5){
	    denom_all_l1m5+=m11;
	    num_all_l1m5+=m21;;
	  }
	  if(imodule==6){
	    denom_all_l1m6+=m11;
	    num_all_l1m6+=m21;;
	  }
	  if(imodule==7){
	    denom_all_l1m7+=m11;
	    num_all_l1m7+=m21;;
	  }
//	  chisq+=pow(res_x_l1/xe1,2)+pow(res_y_l1/ye1,2);
	hx_layer[1]->Fill(res_x_l1);
	hy_layer[1]->Fill(res_y_l1);
	hx[1][imodule]->Fill(res_x_l1);
	hy[1][imodule]->Fill(res_y_l1);
	nsp++;
      }

      hn_aver->Fill(m_sp_track_module->size());
    }

    std::cout<<"make plost"<<std::endl;
    TLatex tex; tex.SetNDC();
    tex.SetTextSize(0.03);
    TF1* fx=new TF1("fx","gaus(0)",-4,4);
    TF1* fy=new TF1("fy","gaus(0)",-0.1,0.1);
    TF1* fp=new TF1("fp","pol1(0)");

    for(int i=1;i<2;i++){
      TString namexlayer=Form("sp_residualX_station0_layer%d",i);
      TCanvas* cxlayer=new TCanvas(namexlayer,namexlayer);
      hx_layer[i]->Draw("E");
      tex.DrawLatex(0.2,0.8,namexlayer);
      fx->SetParameters(10000.,0.0,4.00);
      hx_layer[i]->Fit(fx,"L","",-4.0,4.0);
      tex.DrawLatex(0.15,0.75,Form("#chi^{2} / NDF = %f / %d ", fx->GetChisquare(), fx->GetNDF()));
      tex.DrawLatex(0.15,0.70,Form("mean = %f #pm %f ", fx->GetParameter(1), fx->GetParError(1)));
      tex.DrawLatex(0.15,0.65,Form("sigma = %f #pm %f ", fx->GetParameter(2), fx->GetParError(2)));
      cxlayer->Update();
      cxlayer->SaveAs("figure/"+namexlayer+".png");

      TString nameylayer=Form("sp_residualY_station0_layer%d",i);
      TCanvas* cylayer=new TCanvas(nameylayer,nameylayer);
      hy_layer[i]->Draw("E");
      fy->SetParameters(10000.,0.01,0.44);
      hy_layer[i]->Fit(fy,"L","",-0.1,0.1);
      tex.DrawLatex(0.15,0.8,nameylayer);
      tex.DrawLatex(0.15,0.75,Form("#chi^{2} / NDF = %f / %d ", fy->GetChisquare(), fy->GetNDF()));
      tex.DrawLatex(0.15,0.70,Form("mean = %f #pm %f ", fy->GetParameter(1), fy->GetParError(1)));
      tex.DrawLatex(0.15,0.65,Form("sigma = %f #pm %f ", fy->GetParameter(2), fy->GetParError(2)));
      cylayer->Update();
      cylayer->SaveAs("figure/"+nameylayer+".png");
	//    align_x[i]+=0.-fx->GetParameter(1);
	//    align_y[i]+=0.-fy->GetParameter(1);

      for(int j=0;j<8;j++){
	TString namexmodule=Form("sp_residualX_station0_layer%d_module%d",i,j);
	TCanvas* cxmodule=new TCanvas(namexmodule,namexmodule);
	hx[i][j]->Draw("E");
	tex.DrawLatex(0.2,0.8,namexmodule);
	fx->SetParameters(10000.,0.0,4.00);
	hx[i][j]->Fit(fx,"L","",-4.0,4.0);
	tex.DrawLatex(0.15,0.75,Form("#chi^{2} / NDF = %f / %d ", fx->GetChisquare(), fx->GetNDF()));
	tex.DrawLatex(0.15,0.70,Form("mean = %f #pm %f ", fx->GetParameter(1), fx->GetParError(1)));
	tex.DrawLatex(0.15,0.65,Form("sigma = %f #pm %f ", fx->GetParameter(2), fx->GetParError(2)));
	cxmodule->Update();
	cxmodule->SaveAs("figure/"+namexmodule+".png");

	TString nameymodule=Form("sp_residualY_station0_layer%d_module%d",i,j);
	TCanvas* cymodule=new TCanvas(nameymodule,nameymodule);
	hy[i][j]->Draw("E");
	fy->SetParameters(10000.,0.01,0.44);
	hy[i][j]->Fit(fy,"L","",-0.1,0.1);
	tex.DrawLatex(0.15,0.8,nameymodule);
	tex.DrawLatex(0.15,0.75,Form("#chi^{2} / NDF = %f / %d ", fy->GetChisquare(), fy->GetNDF()));
	tex.DrawLatex(0.15,0.70,Form("mean = %f #pm %f ", fy->GetParameter(1), fy->GetParError(1)));
	tex.DrawLatex(0.15,0.65,Form("sigma = %f #pm %f ", fy->GetParameter(2), fy->GetParError(2)));
	cymodule->Update();
	cymodule->SaveAs("figure/"+nameymodule+".png");
	  //	  align_mx[j]+=0.-fx->GetParameter(1);
	  //	  align_my[j]+=0.-fy->GetParameter(1);
      }

    }

    //get the alignemnt constants
    //for layers
    double inte=1.;
    std::cout<<"test the matrix for l0"<<std::endl;
    //    denom_all_l0.Print();
    //    num_all_l0.Print();
//    TMatrixD inv0=denom_all_l0.Invert(&inte);
//    TMatrixD ma0=inv0*num_all_l0;
//    output<<"result for iteration "<<iter<<std::endl;
    //    inv0.Print();
    //    ma0.Print();
    denom_all_l1.Print();
    num_all_l1.Print();
    std::cout<<"inte 0 "<<inte<<std::endl;
    inte=1.;
    std::cout<<"inte 1 "<<inte<<std::endl;
    TMatrixD inv1=denom_all_l1.Invert(&inte);
    TMatrixD ma1=inv1*num_all_l1;
    inv1.Print();
    ma1.Print();
    std::cout<<"inte 1a "<<inte<<std::endl;
    inte=1.;
    std::cout<<"inte 2b "<<inte<<std::endl;
    TMatrixD inv2=denom_all_l2.Invert(&inte);
    TMatrixD ma2=inv2*num_all_l2;
    std::cout<<"inte 2a "<<inte<<std::endl;
    std::cout<<"for layer 0"<<std::endl;
    ma0.Print();
    //    align_x[0]+=ma0[0][0];
    //    align_y[0]+=ma0[1][0];
    std::cout<<"errors "<<std::endl;
    std::cout<<"for layer 1"<<std::endl;
    ma1.Print();
    align_x[1]+=ma1[0][0];
    align_y[1]+=ma1[1][0];
    /*
       //for modules
      double inte=1.;
      TMatrixD invm0=denom_all_l1m0.Invert(&inte);
      TMatrixD mma0=invm0*num_all_l1m0;
      inte=1.;
      TMatrixD invm1=denom_all_l1m1.Invert(&inte);
      TMatrixD mma1=invm1*num_all_l1m1;
      inte=1.;
      TMatrixD invm2=denom_all_l1m2.Invert(&inte);
      TMatrixD mma2=invm2*num_all_l1m2;
      inte=1.;
      TMatrixD invm3=denom_all_l1m3.Invert(&inte);
      TMatrixD mma3=invm3*num_all_l1m3;
      inte=1.;
      TMatrixD invm4=denom_all_l1m4.Invert(&inte);
      TMatrixD mma4=invm4*num_all_l1m4;
      inte=1.;
      TMatrixD invm5=denom_all_l1m5.Invert(&inte);
      TMatrixD mma5=invm5*num_all_l1m5;
      inte=1.;
      TMatrixD invm6=denom_all_l1m6.Invert(&inte);
      TMatrixD mma6=invm6*num_all_l1m6;
      inte=1.;
      TMatrixD invm7=denom_all_l1m7.Invert(&inte);
      TMatrixD mma7=invm7*num_all_l1m7;
      inte=1.;
      align_mx[0]+=mma0[0][0];
      align_my[0]+=mma0[1][0];
      align_mx[1]+=mma1[0][0];
      align_my[1]+=mma1[1][0];
      align_mx[2]+=mma2[0][0];
      align_my[2]+=mma2[1][0];
      align_mx[3]+=mma3[0][0];
      align_my[3]+=mma3[1][0];
      align_mx[4]+=mma4[0][0];
      align_my[4]+=mma4[1][0];
      align_mx[5]+=mma5[0][0];
      align_my[5]+=mma5[1][0];
      align_mx[6]+=mma6[0][0];
      align_my[6]+=mma6[1][0];
      align_mx[7]+=mma7[0][0];
      align_my[7]+=mma7[1][0];
      std::cout<<"for modules "<<mma0[0][0]<<" "<<mma0[1][0]<<" "<<mma1[0][0]<<" "<<mma1[1][0]<<" "<<mma2[0][0]<<" "<<mma2[1][0]<<" "<<mma3[0][0]<<" "<<mma3[1][0]<<" "<<mma4[0][0]<<" "<<mma4[1][0]<<" "<<mma5[0][0]<<" "<<mma5[1][0]<<" "<<mma6[0][0]<<" "<<mma6[1][0]<<" "<<mma7[0][0]<<" "<<mma7[1][0]<<" "<<std::endl;
      output<<" "<<align_mx[0]<<" "<<align_mx[1]<<" "<<align_mx[2]<<" "<<align_mx[3]<<" "<<align_mx[4]<<" "<<align_mx[5]<<" "<<align_mx[6]<<" "<<align_mx[7]<<std::endl;
      Yxm[0][iter/2]=align_mx[0];//+align_x[1];
      Yxm[1][iter/2]=align_mx[1];//+align_x[1];
      Yxm[2][iter/2]=align_mx[2];//+align_x[1];
      Yxm[3][iter/2]=align_mx[3];//+align_x[1];
      Yxm[4][iter/2]=align_mx[4];//+align_x[1];
      Yxm[5][iter/2]=align_mx[5];//+align_x[1];
      Yxm[6][iter/2]=align_mx[6];//+align_x[1];
      Yxm[7][iter/2]=align_mx[7];//+align_x[1];
      Yym[0][iter/2]=align_my[0];//+align_y[1];
      Yym[1][iter/2]=align_my[1];//+align_y[1];
      Yym[2][iter/2]=align_my[2];//+align_y[1];
      Yym[3][iter/2]=align_my[3];//+align_y[1];
      Yym[4][iter/2]=align_my[4];//+align_y[1];
      Yym[5][iter/2]=align_my[5];//+align_y[1];
      Yym[6][iter/2]=align_my[6];//+align_y[1];
      Yym[7][iter/2]=align_my[7];//+align_y[1];
      */

    //for each module
    //    std::cout<<"for layer 2"<<std::endl;
    //    ma2.Print();
    //    align_x[2]+=ma2[0][0];
    //    align_y[2]+=ma2[1][0];
    output<<align_x[0]<<" "<<align_y[0]<<" "<<align_x[1]<<" "<<align_y[1]<<" "<<align_x[2]<<" "<<align_y[2]<<std::endl;
    std::cout<<"total SP "<<nsp<<std::endl;

  output.close();
}
